<?php // region section ?>

<div class="jtlb-admin">
    <div class="admin-wrap">
        <div class="title-section">
            <h1 class="page-title">Manage Cities</h1>
        </div>

        <div class="content-block">

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Add City</div>
                        <div class="panel-body">
                            <div class="add-city-form">
                                <form method="post" class="row justify-content-start form-area">
                                    <div class="form-group col-5">
                                        <input type="text" class="form-control" name="city" placeholder="City Name">
                                    </div>
                                    <div class="form-group col-4">
                                        <select class="form-control ajax-region-select" name="region">
                                            <option></option>
                                        </select>
                                    </div>
                                    <div class="form-group col-3">
                                        <input type="hidden" name="action" value="jtlb_add_city">
                                        <?php wp_nonce_field();?>

                                        <button class="btn btn-success btn-block" type="submit">
                                            Add City&nbsp;&nbsp;<i class="fas fa-plus-circle"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table id="cities-table" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%"></table>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-default" id="cities-edit">
                        <div class="panel-body">
                            <form method="post" class="form-area">
                                <div class="form-group row justify-content-between">
                                    <div class="col-6">
                                        <input type="text" class="form-control" name="city" placeholder="City Name">
                                    </div>
                                    <div class="col-5">
                                        <select class="form-control ajax-region-select" name="region">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <input type="hidden" name="old_city"/>
                                        <input type="hidden" name="city_id"/>
                                        <input type="hidden" name="region_id" value="">
                                        <input type="hidden" name="old_region" value="">
                                        <input type="hidden" name="action" value="jtlb_edit_city">
                                        <?php wp_nonce_field();?>

                                        <button class="btn btn-success" type="submit">
                                            Update&nbsp;&nbsp;<i class="fas fa-save"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div class="row justify-content-end">
                                <div class="col-12">
                                    <button class="btn btn-default btn-sm close">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>


<script>cities.init();</script>