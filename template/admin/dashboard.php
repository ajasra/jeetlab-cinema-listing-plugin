<div class="jtlb-admin">
    <div class="admin-wrap">
        <div class="title-section">
            <h1 class="page-title">Cinema Listing Settings</h1>
        </div>

        <div class="content-block">
            <div class="well">
                <h4>Please follow the steps to manage the Regions, Cities and Cinemas</h4>
                <ul>
                    <li>Step 1: Add/Edit all Regions.</li>
                    <li>Step 2: Add/Edit all Cities with corresponding Regions.</li>
                    <li>Step 3: Add/Edit all Theatres with corresponding Cities.</li>
                </ul>
            </div>
            <hr>
            <div class="well">
                <h4>To display the list of cinemas in a post/page create the shorcodes</h4>
                <br>
                <a href="<?php echo admin_url('admin.php?page=cinema_shortcode_new');?>" class="btn btn-primary">Add Shortcode</a>
                <br><br>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-12">
                            <table id="shortcode-table" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>shortcodes.init();</script>