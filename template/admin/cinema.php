<?php // region section ?>

<div class="jtlb-admin">
    <div class="admin-wrap">
        <div class="title-section">
            <h1 class="page-title">Manage Cinemas</h1>
        </div>

        <div class="content-block">
         
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Add Cinemas</div>
                        <div class="panel-body">
                            <div class="add-cinema-form">
                                <form method="post" class="row justify-content-start form-area">
                                    <div class="form-group col-sm-7">
                                        <input type="text" class="form-control" name="cinema" placeholder="Cinema Name">
                                    </div>
                                    <div class="form-group col-sm-5">
                                        <select class="form-control ajax-city-select" name="city">
                                            <option></option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-8">
                                        <input type="text" class="form-control" name="ext_url" placeholder="Cinema Url"/>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <input type="hidden" name="action" value="jtlb_add_cinema">
                                        <?php wp_nonce_field();?>

                                        <button class="btn btn-success btn-block" type="submit">
                                            Add Cinema&nbsp;&nbsp;<i class="fas fa-plus-circle"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table id="cinemas-table" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%"></table>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="panel panel-default" id="cinemas-edit">
                        <div class="panel-body">
                            <form method="post" class="form-area">
                                <div class="row">
                                    <div class="form-group col-sm-7">
                                        <input type="text" class="form-control" name="cinema" placeholder="Cinema Name">
                                    </div>
                                    <div class="form-group col-sm-5">
                                        <select class="form-control ajax-city-select" name="city">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-8">
                                        <input type="text" class="form-control" name="ext_url" placeholder="Cinema Url"/>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <input type="hidden" name="cinema_id"/>
                                        <input type="hidden" name="old_cinema"/>
                                        <input type="hidden" name="old_city"/>
                                        <input type="hidden" name="action" value="jtlb_edit_cinema">
                                        <?php wp_nonce_field();?>

                                        <button class="btn btn-success btn-block" type="submit">
                                            Update&nbsp;&nbsp;<i class="fas fa-save"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div class="row justify-content-end">
                                <div class="col-12">
                                    <button class="btn btn-default btn-sm close">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>


<script>cinemas.init();</script>