<?php // region section ?>

<div class="jtlb-admin">
    <div class="admin-wrap">
        <div class="title-section">
            <h1 class="page-title">Manage Regions</h1>
        </div>

        <div class="content-block">

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Add Region</div>
                        <div class="panel-body">
                            <div class="add-region-form">
                                <form method="post" class="row justify-content-start form-area">
                                    <div class="form-group col-sm-8">
                                        <input type="text" class="form-control" name="region" placeholder="Region Name">
                                    </div>
                                    <input type="hidden" name="action" value="jtlb_add_region">
                                    <?php wp_nonce_field();?>

                                    <div class="form-group col-sm-4">
                                        <button class="btn btn-success btn-block" type="submit">
                                            Add Region&nbsp;&nbsp;<i class="fas fa-plus-circle"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table id="regions-table" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-default" id="region-edit">
                        <div class="panel-body">
                            <form method="post" class="form-area">
                                <div class="form-group col-sm-8">
                                    <input type="text" class="form-control" name="region"
                                           placeholder="Region Name">
                                    <input type="hidden" name="region_id" value="">
                                    <input type="hidden" name="old_region" value="">
                                    <input type="hidden" name="action" value="jtlb_edit_region">
                                    <?php wp_nonce_field();?>
                                </div>
                                <div class="form-group col-sm-4">
                                    <button class="btn btn-success btn-block" type="submit">
                                        Update&nbsp;&nbsp;<i class="fas fa-save"></i>
                                    </button>
                                </div>
                            </form>
                            <div class="row justify-content-end">
                                <div class="col-12">
                                    <button class="btn btn-default btn-sm close">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>


<script>regions.init();</script>