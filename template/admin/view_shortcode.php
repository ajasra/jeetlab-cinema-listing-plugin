<?php
// check if id is present
$check_id = $_GET['id'];
?>

<div class="jtlb-admin">
    <div class="admin-wrap">
        <div class="title-section">
            <h1 class="page-title">
                <?php _e('Veiw / Edit the shortcode', 'jeetlab-framework');?>
            </h1>
            <div class="button-area">
                <a class="btn btn-ghost" href="<?php echo admin_url('admin.php?page=cinema_listing');?>">
                    <?php _e('All shortcodes', 'jeetlab-framework');?>
                </a>
            </div>
        </div>

        <div class="content-block detail">
            <div class="container-fluid">
                <div class="row justify-content-start no-gutters">
                    <div class="col-12 col-xl-8">
                        <?php if (get_post($check_id)) : 
                            $list_post = get_post($check_id);
                            $shortcode = "[jtlbcinema id=$check_id]";
                            ?>
                        <div class="form-area">
                            <form autocomplete="off" id="shortcode-form">

                                <div class="form-group row">
                                    <div class="col-6">
                                        <label>Title</label>
                                        <input type="text" name="title" class="form-control" value="<?php echo $list_post->post_title;?>">
                                    </div>
                                    <div class="col-6">
                                        <label>Shortcode</label>
                                        <input type="text" name="shorcode" class="form-control readonly" value="<?php echo $shortcode;?>" readonly>
                                    </div>
                                </div>

                                <div class="row subtitle-area">
                                    <div class="col-12">
                                        <h5>Add / remove the cinemas</h5>
                                    </div>
                                </div>

                                <div class="form-group row cinemas selectbox">
                                    <?php
                                    global $wpdb;
                                    $table_region = $wpdb->prefix . 'jtlb_region';
                                    $table_city = $wpdb->prefix . 'jtlb_city';
                                    $table_cinema = $wpdb->prefix . 'jtlb_cinema';

                                    $meta_ids = get_post_meta( $check_id, 'jeet_cinemas', true );

                                    $options = '';
                                    
                                    if ($meta_ids) {
                                        foreach ($meta_ids as $meta) {
                                            $id = (int)$meta['id'];
                                            $url = $meta['url'];

                                            $cinema = $wpdb->get_results("
                                                        SELECT
                                                        $table_cinema.id, $table_cinema.name AS cinema, $table_city.name AS city, $table_city.id AS city_id, $table_region.name AS region, $table_cinema.ext_url, $table_cinema.active_flag
                                                        FROM $table_cinema
                                                        INNER JOIN $table_city ON
                                                        $table_cinema.city_id = $table_city.id
                                                        INNER JOIN $table_region ON
                                                        $table_city.region_id = $table_region.id
                                                        WHERE $table_cinema.id = $id LIMIT 1
                                                        ");
                                                                                        
                                            $options = '<option value="'.$id.'" selected>' . stripslashes($cinema[0]->cinema) . ' - ' . stripslashes($cinema[0]->city) . ', ' . stripslashes($cinema[0]->region) . '</option>';
                                            ?>

                                            <div class="col-6 cinema-clone">

                                                <div class="row align-items-center form-group">
                                                    <div class="col">
                                                        <div class="">
                                                            <select class="form-control cinema" name="cinemas[]">
                                                                <option></option>
                                                                <?php echo $options;?>
                                                            </select>
                                                            
                                                            <input type="text" class="form-control url" name="url[]" value="<?php echo $url;?>" placeholder="URL of cinema">
                                                        </div>    
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="btn btn-danger jt-remove">
                                                            <i class="far fa-times-circle"></i>
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>

                                        <?php
                                        }
                                    } else { ?>

                                        <div class="col-6 cinema-clone">
                                            <div class="input-group">
                                                <select class="form-control cinema" name="cinemas[]">
                                                    <option></option>
                                                </select>
                                                <div class="input-group-append">
                                                    <button class="btn btn-danger jt-remove">
                                                        <i class="far fa-times-circle"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    <?php } ?>


                                    
                                </div>

                                <div class="form-group row">
                                    <div class="col-12">
                                        <button class="btn btn-ghost add-cinema">Add&nbsp;&nbsp;<i class="far fa-plus-square"></i></button>
                                    </div>
                                </div>
                                
                                <input type="hidden" name="post_id" value="<?php echo $check_id;?>">

                                <?php wp_nonce_field();?>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg" id="save">Save</button>

                                    <span class="jtlb-loading"></span>
                                </div>
                                
                            </form>

                            <div class="response">
                                <div class="updated"><?php _e('Success.', 'jeetlab-framework');?></div>
                                <div class="error"><?php _e('Error.', 'jeetlab-framework');?></div>
                            </div>
                        </div>
                        <?php else : ?>
                        <div class="form-area not-found justify-content-center align-items-center">
                            <h3>Shortcode not found !</h3>
                            <a class="btn btn-ghost" href="<?php echo admin_url('admin.php?page=cinema_listing');?>">
                                <?php _e('Shortcode List', 'jeetlab-framework');?>
                            </a>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>shortcodes.viewInit();</script>