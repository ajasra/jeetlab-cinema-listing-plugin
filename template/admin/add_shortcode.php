<div class="jtlb-admin">
    <div class="admin-wrap">
        <div class="title-section">
            <h1 class="page-title">
                <?php _e('Create the shortcode', 'jeetlab-framework');?>
            </h1>
            <div class="button-area">
                <a class="btn btn-ghost" href="<?php echo admin_url('admin.php?page=cinema_listing');?>">
                    <?php _e('All shortcodes', 'jeetlab-framework');?>
                </a>
            </div>
        </div>

        <div class="content-block detail">
            <div class="container-fluid">
                <div class="row justify-content-start no-gutters">
                    <div class="col-12 col-xl-8">
                        
                        <div class="form-area">
                            <form autocomplete="off" id="shortcode-form">

                                <div class="form-group row">
                                    <div class="col-6">
                                        <label>Title</label>
                                        <input type="text" name="title" class="form-control" value="">
                                    </div>
                                    <div class="col-6">
                                        <label>Shortcode</label>
                                        <input type="text" name="shorcode" class="form-control readonly" value="" readonly>
                                    </div>
                                </div>

                                <div class="row subtitle-area">
                                    <div class="col-12">
                                        <h5>Add / remove the cinemas</h5>
                                    </div>
                                </div>

                                <div class="form-group row cinemas selectbox">
                                    <div class="col-6 cinema-clone">

                                        <div class="row align-items-center form-group">
                                            <div class="col">
                                                <div class="">
                                                    <select class="form-control cinema" name="cinemas[]">
                                                        <option></option>
                                                    </select>
                                                    
                                                    <input type="text" class="form-control url" name="url[]" placeholder="URL of cinema">
                                                </div>    
                                            </div>
                                            <div class="col-auto">
                                                <button class="btn btn-danger jt-remove">
                                                    <i class="far fa-times-circle"></i>
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-12">
                                        <button class="btn btn-ghost add-cinema">Add&nbsp;&nbsp;<i class="far fa-plus-square"></i></button>
                                    </div>
                                </div>
                                
                                <?php wp_nonce_field();?>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg" id="save">Save</button>

                                    <span class="jtlb-loading"></span>
                                </div>
                                
                            </form>

                            <div class="response">
                                <div class="updated"><?php _e('Success.', 'jeetlab-framework');?></div>
                                <div class="error"><?php _e('Error.', 'jeetlab-framework');?></div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>shortcodes.viewInit();</script>