<?php
/* 
    trigger on uninstall of the plugin
*/

// prevent accidental unistallation of plugin by direct access to file
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}

global $table_prefix, $wpdb;

$table_cinema = $table_prefix . 'jtlb_cinema';
$table_city = $table_prefix . 'jtlb_city';
$table_region = $table_prefix . 'jtlb_region';
$table_post_meta = $table_prefix . 'postmeta';
$table_posts = $table_prefix . 'posts';

$sql = "DROP TABLE IF EXISTS $table_region, $table_city, $table_cinema";
$wpdb->query($sql);

// remove the cpt created
$query = $wpdb->query("DELETE FROM $table_post_meta WHERE post_id IN (SELECT id FROM $table_posts WHERE post_type = 'jtlb-cinema')");
