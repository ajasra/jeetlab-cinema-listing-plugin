<?php
/* 
    All ajax calls for regions, city and cinema CRUD
*/

class JtlbSeedDataCrud
{
    // register the hoocks for the ajax calls
    public static function resgister_the_hooks() {
        // regions
        add_action( 'wp_ajax_jtlb_add_region' , [ 'JtlbSeedDataCrud', 'jtlb_add_region' ] );
        add_action( 'wp_ajax_jtlb_edit_region' , [ 'JtlbSeedDataCrud', 'jtlb_edit_region' ] );
        add_action( 'wp_ajax_jtlb_hide_region' , [ 'JtlbSeedDataCrud', 'jtlb_hide_region' ] );

        // cities
        add_action( 'wp_ajax_jtlb_add_city' , [ 'JtlbSeedDataCrud', 'jtlb_add_city' ] );
        add_action( 'wp_ajax_jtlb_edit_city' , [ 'JtlbSeedDataCrud', 'jtlb_edit_city' ] );
        add_action( 'wp_ajax_jtlb_hide_city' , [ 'JtlbSeedDataCrud', 'jtlb_hide_city' ] );

        // cinemas
        add_action( 'wp_ajax_jtlb_add_cinema' , [ 'JtlbSeedDataCrud', 'jtlb_add_cinema' ] );
        add_action( 'wp_ajax_jtlb_edit_cinema' , [ 'JtlbSeedDataCrud', 'jtlb_edit_cinema' ] );
        add_action( 'wp_ajax_jtlb_hide_cinema' , [ 'JtlbSeedDataCrud', 'jtlb_hide_cinema' ] );
        add_action( 'wp_ajax_jtlb_delete_cinema' , [ 'JtlbSeedDataCrud', 'jtlb_delete_cinema' ] );
    }

    // add region
    static function jtlb_add_region() {
        $success = array();
        $error = array();

        if ($_POST['_wpnonce']) {

            global $wpdb;

            $table = $wpdb->prefix . 'jtlb_region';

            if ($_POST['region'] != '' || $_POST['region'] != null) {
                $reg = $_POST['region'];
                $check = $wpdb->get_var("SELECT COUNT(*) FROM $table WHERE name='$reg'");

                if (!$check) {
                    $query = $wpdb->insert($table,
                        array(
                            'name' => stripslashes($_POST['region']),
                        ),
                        array(
                            '%s'
                        ));

                    if ($query) {
                        array_push($success, 'Added new region successfully.');
                        wp_send_json_success($success);
                    } else {
                        array_push($error, 'Something went wrong.');
                        wp_send_json_error($error);
                    }
                } else {
                    array_push($error, 'Region already exists.');
                    wp_send_json_error($error);
                }
            } else {
                array_push($error, 'Region cannot be empty.');
                wp_send_json_error($error);
            }
        } else {
            array_push($error, 'Are you sure you are human ??');
            wp_send_json_error($error);
        }
    }

    // edit region
    static function jtlb_edit_region() {
        $success = array();
        $error = array();

        if ($_POST['_wpnonce']) {
            global $wpdb;

            $table = $wpdb->prefix . 'jtlb_region';

            if ($_POST['region'] != null) {
                if ($_POST['region'] == $_POST['old_region']) {
                    array_push($error, 'Old and new regions are same.');
                    wp_send_json_error($error);
                } else {
                    $query = $wpdb->update(
                        $table,
                        array(
                            'name' => stripslashes($_POST['region'])
                        ),
                        array(
                            'id' => $_POST['region_id'],
                        ));

                    if ($query) {
                        array_push($success, 'Updated the region successfully');
                        wp_send_json_success($success);
                    } else {
                        array_push($error, 'Something went wrong');
                        wp_send_json_error($error);
                    }
                }
            } else {
                array_push($error, 'Region cannot be empty.');
                wp_send_json_error($error);
            }

        } else {
            array_push($error, 'Are you sure you are human ??');
            wp_send_json_error($error);
        }
    }

    // hide region
    static function jtlb_hide_region() {
        global $wpdb;
        $query = $wpdb->update(
            $wpdb->prefix . 'jtlb_region',
            array(
                'active_flag' => $_POST['active_flag']
            ),
            array(
                'id' => $_POST['id'],
            ));
        
        if ($query) {
            wp_send_json_success();
        } else {
            wp_send_json_error();
        }
    }

    // add city
    static function jtlb_add_city() {
        $success = array();
        $error = array();

        if ($_POST['_wpnonce']) {

            global $wpdb;

            $table = $wpdb->prefix . 'jtlb_city';

            if (isset($_POST['city']) && ($_POST['city'] != '' || $_POST['city'] != null)) {
                if (isset($_POST['region']) && ($_POST['region'] != '' || $_POST['region'] != null)) {
                    $city = $_POST['city'];
                    $region = $_POST['region'];

                    $check = $wpdb->get_var("SELECT COUNT(*) FROM $table WHERE name='$city' AND region_id='$region'");
                    if (!$check) {
                        $query = $wpdb->insert(
                            $wpdb->prefix . 'jtlb_city',
                            array(
                                'name' => stripslashes($_POST['city']),
                                'region_id' => $_POST['region'],
                            ),
                            array(
                                '%s',
                                '%d'
                            ));

                        if ($query) {
                            array_push($success, 'City added successfully!');
                            wp_send_json_success($success);
                        } else {
                            array_push($error, 'Something went wrong.');
                            wp_send_json_error($error);
                        }
                    } else {
                        array_push($error, 'City - Region combination already exists');
                        wp_send_json_error($error);
                    }

                } else {
                    array_push($error, 'Region cannot be empty');
                    wp_send_json_error($error);
                }
            } else {
                array_push($error, 'City cannot be empty.');
                wp_send_json_error($error);
            }

        } else {
            array_push($error, 'Are you sure you are human ??');
            wp_send_json_error($error);
        }
    }

    // edit city
    static function jtlb_edit_city() {
        $success = array();
        $error = array();

        if ($_POST['_wpnonce']) {
            global $wpdb;

            $table = $wpdb->prefix . 'jtlb_city';

            $city = stripslashes($_POST['city']);
            $old_city = $_POST['old_city'];
            $city_id = $_POST['city_id'];
            $region = $_POST['region'];
            $old_region = $_POST['region_id'];
            $city = $_POST['city'];
            if ($city != null) {
                $check = $wpdb->get_var("SELECT COUNT(*) FROM $table WHERE name='$city' AND region_id='$region'");
                if (!$check) {
                    $query = $wpdb->update(
                        $wpdb->prefix . 'jtlb_city',
                        array(
                            'name' => $city,
                            'region_id' => $region,
                        ),
                        array(
                            'id' => $city_id,
                        ));

                    if ($query) {
                        array_push($success, 'Updated the city successfully!');
                        wp_send_json_success($success);                        
                    } else {
                        array_push($error, 'Something went wrong');
                        wp_send_json_error($error);
                    }
                } else {
                    array_push($error, 'City - Region combination already exists');
                    wp_send_json_error($error);
                }

            } else {
                array_push($error, 'City cannot be empty');
                wp_send_json_error($error);
            }
        } else {
            array_push($error, 'Are you sure you are human ??');
            wp_send_json_error($error);
        }
    }

    // hide region
    static function jtlb_hide_city() {
        global $wpdb;
        $query = $wpdb->update(
            $wpdb->prefix . 'jtlb_city',
            array(
                'active_flag' => $_POST['active_flag']
            ),
            array(
                'id' => $_POST['id'],
            ));
        
        if ($query) {
            wp_send_json_success();
        } else {
            wp_send_json_error();
        }
    }

    // add cinema
    static function jtlb_add_cinema() {
        $success = array();
        $error = array();

        if ($_POST['_wpnonce']) {

            global $wpdb;

            if (isset($_POST['cinema']) && ($_POST['cinema'] != '' || $_POST['cinema'] != null)) {
                if (isset($_POST['city']) && ($_POST['city'] != '' || $_POST['city'] != null)) {
                $query = $wpdb->replace(
                    $wpdb->prefix . 'jtlb_cinema',
                    array(
                        'name' => stripslashes($_POST['cinema']),
                        'city_id' => $_POST['city'],
                        'ext_url' => $_POST['ext_url'],
                    ),
                    array(
                        '%s',
                        '%d',
                        '%s'
                    ));

                if ($query) {
                    array_push($success, 'Added new cinema successfully!');
                    wp_send_json_success($success);
                } else {
                    array_push($error, 'Something went wrong.');
                    wp_send_json_error($error);
                }

                } else {
                    array_push($error, 'City cannot be empty');
                    wp_send_json_error($error);
                }
            } else {
                array_push($error, 'Cinema cannot be empty');
                wp_send_json_error($error);
            }
        } else {
            array_push($error, 'Are you sure you are human ??');
            wp_send_json_error($error);
        }
    }

    // edit cinema 
    static function jtlb_edit_cinema() {
        $success = array();
        $error = array();

        if ($_POST['_wpnonce']) {
            global $wpdb;

            if (isset($_POST['cinema']) && ($_POST['cinema'] != '' || $_POST['cinema'] != null)) {
                if (isset($_POST['city']) && ($_POST['city'] != '' || $_POST['city'] != null)) {
                    
                    $query = $wpdb->update(
                        $wpdb->prefix . 'jtlb_cinema',
                        array(
                            'name'      => stripslashes($_POST['cinema']),
                            'city_id'   => $_POST['city'],
                            'ext_url'   => $_POST['ext_url'],
                        ),
                        array(
                            'id' => $_POST['cinema_id']
                        ),
                        array(
                            '%s',
                            '%d',
                            '%s'
                        ),
                        array(
                            '%d'
                        )
                    );

                    if ($query) {
                        array_push($success, 'Updated cinema successfully!');
                        wp_send_json_success($success);
                    } elseif ($query == 0) {
                        array_push($success, 'Updated cinema with same data');
                        wp_send_json_success($success);
                    } else {
                        array_push($error, 'Something went wrong.');
                        wp_send_json_error($error);
                    }

                } else {
                    array_push($error, 'City cannot be empty');
                    wp_send_json_error($error);
                }
            } else {
                array_push($error, 'Cinema cannot be empty');
                wp_send_json_error($error);
            }

        } else {
            array_push($error, 'Are you sure you are human ??');
            wp_send_json_error($error);
        }
    }

    // hide cinema
    static function jtlb_hide_cinema() {
        global $wpdb;
        $query = $wpdb->update(
            $wpdb->prefix . 'jtlb_cinema',
            array(
                'active_flag' => $_POST['active_flag']
            ),
            array(
                'id' => $_POST['id'],
            ));

        if ($query) {
            wp_send_json_success();
        } else {
            wp_send_json_error();
        } 
    }

    // delete cinema
    static function jtlb_delete_cinema() {
        global $wpdb;
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $table = $wpdb->prefix . 'jtlb_cinema';

            $remove = $wpdb->delete($table, array('id' => $id), array('%d'));

            if ($remove) {
                wp_send_json_success();
            } else {
                wp_send_json_error();
            }
        } else {
            wp_send_json_error();
        }
    }
}