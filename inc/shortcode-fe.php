<?php
/* 
    this creates the front end of the shortcode
*/

class JtlbShortcodeRender
{
    // call the shortcode hook
    public static function shortcode_action() {
        add_shortcode ( 'jtlbcinema', array( 'JtlbShortcodeRender', 'cinema_list_shortcode' ) );

        
    }

    // create shortcode 
    static function cinema_list_shortcode($atts) {
        // get the post id
        $post_id = $atts['id'];

        // get the cinema ids from meta 
        $meta_ids = get_post_meta( $post_id, 'jeet_cinemas', true );

        $booking = $meta_ids;


        global $wpdb;
        $table_region = $wpdb->prefix . 'jtlb_region';
        $table_city = $wpdb->prefix . 'jtlb_city';
        $table_cinema = $wpdb->prefix . 'jtlb_cinema';


        if ($meta_ids) {

            $cinemalist_obj = new stdClass();

            // create the cinema object
            $cinemas = array();
            foreach ($meta_ids as $cinema) {

                $cinema_id = $cinema['id'];

                $cinema_row = $wpdb->get_results("SELECT * FROM $table_cinema WHERE id=$cinema_id AND active_flag=1");

                $obj = new stdClass();
                $obj->id = $cinema_row[0]->id;
                $obj->name = $cinema_row[0]->name;
                $obj->url = ($cinema['url']) ? $cinema['url'] : '#';
                $obj->city_id = $cinema_row[0]->city_id;

                array_push($cinemas, $obj);
            }
            $cinemalist_obj->cinemas = $cinemas;


            // create the cities object
            $all_cities = array();
            foreach ($cinemas as $value) {
                $city_id = $value->city_id;
                $city_row = $wpdb->get_results("SELECT * FROM $table_city WHERE id=$city_id");

                $obj = new stdClass();
                $obj->id = $city_row[0]->id;
                $obj->text = $city_row[0]->name;
                $obj->region_id = $city_row[0]->region_id;

                array_push($all_cities, $obj);
            }
            // create unique cities
            /* refer - https://gist.github.com/kellenmace/77f55e70a06cec7a3a61b7cd6f8a3081 */
            $city_map = array_map(function($item) {
                return $item->id;
            }, $all_cities);

            $unique_cities = array_unique($city_map);

            $cities = array_values( array_intersect_key( $all_cities, $unique_cities ) );
            $cinemalist_obj->cities = $cities;


            // create the region object
            $all_regions = array();
            foreach ($cities as $value) {
                $region_id = $value->region_id;
                $region_row = $wpdb->get_results("SELECT * FROM $table_region WHERE id=$region_id");

                $obj = new stdClass();
                $obj->id = $region_row[0]->id;
                $obj->text = $region_row[0]->name;

                array_push($all_regions, $obj);
            }
            // create unique regions
            $region_map = array_map(function($item) {
                return $item->id;
            }, $all_regions);

            $unique_regions = array_unique($region_map);

            $regions = array_values( array_intersect_key( $all_regions, $unique_regions ) );

            $cinemalist_obj->regions = $regions;
        }

        // append the scripts
        wp_enqueue_style('cinema-select2-css', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/css/select2.min.css', '4.0.9');
        wp_enqueue_script('cinema-lodash', '//cdn.jsdelivr.net/npm/lodash@4.17.15/lodash.min.js', '', '4.17.15', false);
        wp_enqueue_script('cinema-select2', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js', '', '4.0.9', false);
        wp_enqueue_script('cinema-select2-it', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/i18n/it.js', '', '4.0.9', false);
        wp_enqueue_script('cinema-js', plugins_url('../js/jtlbapp.js', __FILE__), '', '1.0.0', false);
        wp_localize_script('cinema-js', 'WPURLS', array(
            'cinemalist_obj'    => $cinemalist_obj
        ));

        self::create_the_markup();
    }

    // create the markup for form
    static function create_the_markup() {
        ?>

        <div class="booking-infos row justify-content-center">
            <form class="region-select col-12">
                <div class="form-group">
                    <label>Regione</label>
                    <select class="form-control region" id="jtlb-region" name="region" placeholder="region">
                        <option></option>
                    </select>
                </div>
            </form>
            
            <form class="city-select col-12" style="display:none;">
                <div class="form-group">
                    <label>Città</label>
                    <select class="form-control city" id="jtlb-city" name="city">
                        <option></option>
                    </select>
                </div>
            </form>

            <div class="cinema-select col-12" style="display:none;">
                <div class="form-group">
                    <label>Cinema</label>
                    <div class="cinema-wrapper" id="jtlb-cinema">
                        <ul class="cinema"></ul>
                    </div>
                </div>
            </div>
        </div>
        
        <?php
    }
}