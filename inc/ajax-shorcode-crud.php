<?php
/* 
    All ajax calls for shortcode CRUD
*/


class JtlbAjaxShortcodeCrud
{
    // register the hoocks for the ajax calls
    public static function resgister_the_hooks() {
        add_action( 'wp_ajax_jtlb_get_cinemas_for_shortcodes' , [ 'JtlbAjaxShortcodeCrud', 'jtlb_get_cinemas_for_shortcodes' ] );
        add_action( 'wp_ajax_jtlb_edit_cinema_shortcode' , [ 'JtlbAjaxShortcodeCrud', 'jtlb_edit_cinema_shortcode' ] );
        add_action( 'wp_ajax_jtlb_remove_cinema_shortcode' , [ 'JtlbAjaxShortcodeCrud', 'jtlb_remove_cinema_shortcode' ] );
    }


    // get the cinemas for shortcodes
    static function jtlb_get_cinemas_for_shortcodes() {
        global $wpdb;
        $table_region = $wpdb->prefix . 'jtlb_region';
        $table_city = $wpdb->prefix . 'jtlb_city';
        $table_cinema = $wpdb->prefix . 'jtlb_cinema';

        $ids = $_POST['ids'];

        $where_id = '';

        if ($ids) {
            $trimmed_ids = rtrim($ids, ',');
            $where_id = " AND $table_cinema.id NOT IN ($trimmed_ids)";
        }

        $all_cinemas = $wpdb->get_results("
                        SELECT
                        $table_cinema.id, $table_cinema.name AS cinema, $table_city.name AS city, $table_city.id AS city_id, $table_region.name AS region, $table_cinema.ext_url, $table_cinema.active_flag
                        FROM $table_cinema
                        INNER JOIN $table_city ON
                        $table_cinema.city_id = $table_city.id
                        INNER JOIN $table_region ON
                        $table_city.region_id = $table_region.id
                        WHERE $table_cinema.active_flag = 1 $where_id
                        ");

        $output = array();
        foreach ($all_cinemas as $item) {
            $value = stripslashes($item->cinema) . ' - ' . stripslashes($item->city) . ', ' . stripslashes($item->region);
            
            $obj = new stdClass();
            $obj->id = $item->id;
            $obj->text = $value;
            $obj->url = $item->ext_url;

            array_push($output, $obj);
        }

        wp_send_json($output);
    }

    // save the cinema shortcode 
    static function jtlb_edit_cinema_shortcode() {
        parse_str($_POST['formData'], $formData);

        $error = array();
        $success = array();

        if (wp_verify_nonce($formData['_wpnonce'])) {
            if ($formData['post_id']) {
                // save the post
                $save_post = wp_insert_post(array(
                    'post_status'   => 'publish',
                    'post_type'     => 'jtlb-cinema',
                    'post_author'   => get_current_user_id(),
                    'post_title'    => $formData['title'],
                    'ID'            => $formData['post_id']
                ));
                $post_id = $formData['post_id'];
            } else {
                // save the post
                $save_post = wp_insert_post(array(
                    'post_status'   => 'publish',
                    'post_type'     => 'jtlb-cinema',
                    'post_author'   => get_current_user_id(),
                    'post_title'    => $formData['title'],
                ));

                $post_id = $save_post;
            }

            // create the array of merged cinema id and url
            $merged_array = array();
            foreach ($formData['cinemas'] as $key => $value) {
                $obj = array(
                    'id' => $value,
                    'url' => $formData['url'][$key]
                );

                array_push($merged_array, $obj);
            }

            // save the metabox
            if (!add_post_meta($post_id, 'jeet_cinemas', $merged_array, true)) {
                update_post_meta($post_id, 'jeet_cinemas', $merged_array);
            }

            if ($save_post) {
                // if not exiting post then send post id
                if (!$formData['post_id']) {
                    $msg = array(
                        'id' => $post_id, 
                        'url' => admin_url('admin.php?page=cinema_shortcode_view').'&id='.$post_id
                    );
                    array_push($success, $msg);
                    wp_send_json_success($success);
                } else {
                    $msg = 'Shortcode saved successfully.';
                    array_push($success, $msg);
                    wp_send_json_success($success);
                }
            } else {
                array_push($error, 'Something went wrong. Please try again.');
                wp_send_json_error($error);
            }
        } else {
            array_push($error, 'Are you sure you are human ? Please try again !!.');
            wp_send_json_error($error);
        }
    }

    // remove the shortcode post
    static function jtlb_remove_cinema_shortcode() {
        
        if ($_POST['entry_id']) {
            // remove the post
            wp_delete_post( $_POST['entry_id'], true );

            // remove the meta
            delete_post_meta( $_POST['entry_id'], 'jeet_cinemas' );
        }
        // wp_send_json( $_POST['entry_id'] );
    }
}