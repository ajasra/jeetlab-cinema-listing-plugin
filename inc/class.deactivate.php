<?php
/* 
    activation class for the plugin
*/

class JtlbCinemaPluginDeactivate {
    
    public static function deactivate() {
        // flush rewrite rules after deactivate
        flush_rewrite_rules();
    }
}