<?php
/* 
    activation class for the plugin
*/

class JtlbCinemaPluginActivate
{
    public static function activate() {
        // flush rewrite rules after init
        flush_rewrite_rules();

        // create the db tables
        require_once dirname(__FILE__) . '/installer.php';
    }    
}
