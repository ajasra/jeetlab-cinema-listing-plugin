<?php
/* 
    All ajax calls to fetch the lists
*/

class JtlbAjaxListing
{    
    // register the hoocks for the ajax calls
    public static function resgister_the_hooks() {
        add_action( 'wp_ajax_jtlb_list_shortcodes' , [ 'JtlbAjaxListing', 'jtlb_list_shortcodes' ] );
        add_action( 'wp_ajax_jtlb_list_regions' , [ 'JtlbAjaxListing', 'jtlb_list_regions' ] );
        add_action( 'wp_ajax_jtlb_list_cities' , [ 'JtlbAjaxListing', 'jtlb_list_cities' ] );
        add_action( 'wp_ajax_jtlb_list_cinemas' , [ 'JtlbAjaxListing', 'jtlb_list_cinemas' ] );
    }
    
    // get list of the shortcodes
    static function jtlb_list_shortcodes() {
        $args = array(
            'post_type' => 'jtlb-cinema',
            'posts_per_page' => -1
        );

        $query = new WP_Query($args);

        $output = array();

        if ($query->have_posts()) {
            $count = 1;
            while ($query->have_posts()) {
                $query->the_post();

                $obj = new stdClass();
                $obj->count = $count++;
		        $obj->id = get_the_ID();
                $obj->name = get_the_title();
                $obj->shortcode = '[jtlbcinema id='.get_the_ID().']';

                $view = admin_url('admin.php?page=cinema_shortcode_view').'&id='.get_the_ID();
                $view_btn = '<a class="btn btn-sm view" href="'.$view.'"><i class="fas fa-eye"></i> View</a>';
                
                $actions = '<div class="buttons">'.$view_btn.'
                                <button class="btn btn-sm remove" data-id="'.get_the_ID().'"><i class="fas fa-trash"></i></button>
                            </div>';

                $obj->actions = $actions;

                array_push($output, $obj);
            }        
        }

        wp_send_json($output);
    }

    // get list of regions from seed
    static function jtlb_list_regions() {
        global $wpdb;

        $table_region = $wpdb->prefix . 'jtlb_region';

        $all_regions = $wpdb->get_results("SELECT * FROM $table_region");

        if (isset($_POST['active']) && $_POST['active'] == true) {
            $all_regions = $wpdb->get_results("SELECT * FROM $table_region WHERE active_flag=1");
        }

        $response = array();

        $count = 1;
        foreach ($all_regions as $item) {
            $obj = new stdClass();
            $obj->count = $count++;
            $obj->id = $item->id;
            $obj->region = stripslashes($item->name);
            $active_flag = ($item->active_flag == 1) ? 0 : 1;
            $active = <<<BTN
                    <button class="btn btn-danger btn-xs show-hide"
                    data-id="{$item->id}"
                    data-active="{$active_flag}"
                    data-region="{$item->name}"
                    data-loading-text="Hiding...">Hide <i class="fas fa-eye-slash"></i></button>
BTN;
            $inactive = <<<BTN
                    <button class="btn btn-success btn-xs show-hide"
                    data-id="{$item->id}"
                    data-active="{$active_flag}"
                    data-region="{$item->name}"
                    data-loading-text="Showing...">Show <i class="fas fa-eye"></i></button>
BTN;
            $edit = <<<BTN
                    <button class="btn btn-info btn-xs edit"
                    data-id="{$item->id}"
                    data-region="{$item->name}">Edit <i class="fas fa-edit"></i></button>
BTN;
            $obj->edit = $edit;
            $obj->active = ($item->active_flag == 1) ? $active : $inactive;

            array_push($response, $obj);
        }

        wp_send_json($response);
    }

    // get list of cities from seed 
    static function jtlb_list_cities() {
        global $wpdb;

        $table_region = $wpdb->prefix . 'jtlb_region';
        $table_city = $wpdb->prefix . 'jtlb_city';

        $all_cities = $wpdb->get_results("
                            SELECT
                            $table_city.id, $table_city.name AS city, $table_region.name AS region, $table_region.id AS region_id, $table_city.active_flag
                            FROM $table_city
                            INNER JOIN $table_region ON
                            $table_city.region_id = $table_region.id
                            ");

        if (isset($_GET['active']) && $_GET['active'] == true) {
            $all_cities = $wpdb->get_results("
                            SELECT
                            $table_city.id, $table_city.name AS city, $table_region.name AS region, $table_region.id AS region_id, $table_city.active_flag
                            FROM $table_city
                            INNER JOIN $table_region ON
                            $table_city.region_id = $table_region.id
                            WHERE $table_city.active_flag = 1
                            ");
        }

        $response = array();

        $count = 1;
        foreach ($all_cities as $item) {
            $obj = new stdClass();
            $obj->count = $count++;
            $obj->id = $item->id;
            $obj->city = stripslashes($item->city);
            $obj->region = stripslashes($item->region);
            $obj->region_id = $item->region_id;
            $active_flag = ($item->active_flag == 1) ? 0 : 1;
            $active = <<<BTN
                    <button class="btn btn-danger btn-xs show-hide"
                    data-id="{$item->id}"
                    data-active="{$active_flag}"
                    data-loading-text="Hiding...">Hide <i class="fas fa-eye-slash"></i></button>
BTN;
            $inactive = <<<BTN
                    <button class="btn btn-success btn-xs show-hide"
                    data-id="{$item->id}"
                    data-active="{$active_flag}"
                    data-loading-text="Showing...">Show <i class="fas fa-eye"></i></button>
BTN;

            $edit = <<<BTN
                    <button class="btn btn-info btn-xs edit"
                    data-id="{$item->id}"
                    data-city="{$item->city}"
                    data-region="{$item->region}"
                    data-region_id="{$item->region_id}"
                    >Edit <i class="fas fa-edit"></i></button>
BTN;
            $obj->edit = $edit;
            $obj->active = ($item->active_flag == 1) ? $active : $inactive;

            array_push($response, $obj);
        }

        wp_send_json($response);
    }


    // get the list of cinemas from seed
    static function jtlb_list_cinemas() {
        global $wpdb;

        $table_region = $wpdb->prefix . 'jtlb_region';
        $table_city = $wpdb->prefix . 'jtlb_city';
        $table_cinema = $wpdb->prefix . 'jtlb_cinema';

        $all_cinemas = $wpdb->get_results("
                SELECT
                $table_cinema.id, $table_cinema.name AS cinema, $table_city.name AS city, $table_city.id AS city_id, $table_region.name AS region, $table_cinema.ext_url, $table_cinema.active_flag
                FROM $table_cinema
                INNER JOIN $table_city ON
                $table_cinema.city_id = $table_city.id
                INNER JOIN $table_region ON
                $table_city.region_id = $table_region.id
                ");

        if (isset($_POST['active']) && $_POST['active'] == true) {
            $all_cinemas = $wpdb->get_results("
                SELECT
                $table_cinema.id, $table_cinema.name AS cinema, $table_city.name AS city, $table_city.id AS city_id, $table_region.name AS region, $table_cinema.ext_url, $table_cinema.active_flag
                FROM $table_cinema
                INNER JOIN $table_city ON
                $table_cinema.city_id = $table_city.id
                INNER JOIN $table_region ON
                $table_city.region_id = $table_region.id
                WHERE $table_cinema.active_flag = 1
                ");
        }

        $response = array();

        $count = 1;
        foreach ($all_cinemas as $item) {
            $obj = new stdClass();
            $obj->count = $count++;
            $obj->id = $item->id;
            $obj->cinema = stripslashes($item->cinema);
            $obj->city = stripslashes($item->city);
            $obj->city_id = $item->city_id;
            $obj->region = stripslashes($item->region);
            $obj->ext_url = $item->ext_url;
            $active_flag = ($item->active_flag == 1) ? 0 : 1;
            if ($item->ext_url) {
                $url = <<<BTN
                    <a href="{$item->ext_url}" class="btn btn-xs btn-default" target="_blank">
                        Url <i class="fas fa-share-square"></i>
                    </a>
BTN;
            } else {
                $url = '--';
            }
            $obj->url = $url;

            $active = <<<BTN
                    <button class="btn btn-danger btn-xs show-hide"
                    data-id="{$item->id}"
                    data-active="{$active_flag}"
                    data-loading-text="Hiding...">Hide <i class="fas fa-eye-slash"></i></button>
BTN;
            $inactive = <<<BTN
                    <button class="btn btn-success btn-xs show-hide"
                    data-id="{$item->id}"
                    data-active="{$active_flag}"
                    data-loading-text="Showing...">Show <i class="fas fa-eye"></i></button>
BTN;
            $edit = <<<BTN
                    <button class="btn btn-info btn-xs edit"
                    data-id="{$item->id}"
                    data-cinema="{$item->cinema}"
                    data-city="{$item->city}"
                    data-city_id="{$item->city_id}"
                    data-ext_url="{$item->ext_url}"
                    >Edit <i class="fas fa-edit"></i></button>
BTN;
            $obj->edit = $edit;
            $obj->active = ($item->active_flag == 1) ? $active : $inactive;

            $del = <<<BTN
                    <button class="btn btn-danger btn-xs delete"
                    data-id="{$item->id}"
                    data-cinema="{$item->cinema}"
                    >Delete <i class="fas fa-trash"></i></button>
BTN;

            $obj->del = $del;

            array_push($response, $obj);
        }

        wp_send_json($response);
    }
}
