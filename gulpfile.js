// Dependencies
var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    concatCss = require('gulp-concat-css'),
    cssmin = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer'),
    maps = require('gulp-sourcemaps'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    uglify = require('gulp-uglify'),
    minify = require('gulp-minify'),
    concat = require('gulp-concat'),
    replace = require('gulp-replace'),
    replaceBatch = require('gulp-batch-replace'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    clean = require('gulp-clean'),
    gulpSequence = require('gulp-sequence'),
    browserSync = require('browser-sync').create(),
    babel = require('gulp-babel'),
    debug = require('gulp-debug');
var reload = browserSync.reload;


const LOCAL_SITE = 'http://localhost:8888/plugin/';

// the source files to work on
var inputs = require('./asset_source');

// sass
gulp.task('sass', function () {
    return gulp.src(inputs.scss)
        .pipe(plumber())
        .pipe(maps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(maps.write('../srcmaps'))
        .pipe(plumber.stop())
        .pipe(gulp.dest('css'))
        .pipe(notify({
            message: 'Compiled SCSS!'
        }))
        .pipe(reload({
            stream: true
        }));
});
// admin-sass
gulp.task('admin-sass', function () {
    return gulp.src(inputs.admin_scss)
        .pipe(plumber())
        .pipe(maps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(maps.write('../srcmaps'))
        .pipe(plumber.stop())
        .pipe(gulp.dest('css'))
        .pipe(notify({
            message: 'Compiled Admin SCSS!'
        }))
        .pipe(reload({
            stream: true
        }));
});
// minify sass
gulp.task('minify-sass', ['sass'], function () {
    return gulp.src('css/app.css')
        .pipe(plumber())
        .pipe(cssmin())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest('css'))
        .pipe(notify({
            message: 'Minified SCSS'
        }));
});
// concat all css libraries, fix relative links to font-awesome fonts and minify the css.
gulp.task('csslibs', function () {
    return gulp.src(inputs.libCss)
        .pipe(debug())
        .pipe(plumber())
        .pipe(maps.init())
        .pipe(concatCss('libs.css'))
        .pipe(maps.write('../srcmaps'))
        .pipe(plumber.stop())
        .pipe(gulp.dest('css'))
        .pipe(notify({
            message: 'Concatenation of css libraries'
        }));
});
// minify all css libs
gulp.task('minify-csslibs', ['csslibs'], function () {
    return gulp.src('css/libs.css')
        .pipe(plumber())
        .pipe(cssmin())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest('css'))
        .pipe(notify({
            message: 'Minified css libraries'
        }));
});

// js libraries
gulp.task('jslibs', function () {
    return gulp.src(inputs.libs)
        .pipe(gulp.dest('js/vendor'))
        .pipe(notify({
            message: 'Moving of js libraries done'
        }));
});
// js scripts
gulp.task('jsscripts', function () {
    return gulp.src(inputs.scripts)
        .pipe(plumber())
        .pipe(maps.init())
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(babel())
        .pipe(concat('jtlbapp.js'))
        .pipe(maps.write('../srcmaps'))
        .pipe(gulp.dest('js'))
        .pipe(plumber.stop())
        .pipe(notify({
            message: 'Compiled es6 to es5'
        }))
        .pipe(reload({
            stream: true
        }));
});

// js scripts
gulp.task('jsscripts_admin', function () {
    return gulp.src('scripts/admin.js')
        .pipe(plumber())
        .pipe(maps.init())
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(babel())
        .pipe(maps.write('../srcmaps'))
        .pipe(gulp.dest('js'))
        .pipe(plumber.stop())
        .pipe(notify({
            message: 'Compliled es6 to es5 of admin scripts'
        }));
});
// minify js scripts
gulp.task('minify-js', ['jsscripts', 'jsscripts_admin'], function () {
    return gulp.src(['js/jtlbapp.js', 'js/admin.js'])
        .pipe(plumber())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify({
            output: {
                comments: '/^!/'
            }
        }))
        .pipe(gulp.dest('js'))
        .pipe(plumber.stop())
        .pipe(notify({
            message: 'Minification of custom scripts done'
        }));
});


// watch
gulp.task('watch', function () {
    gulp.watch('scss/**/*.scss', ['sass', 'admin-sass']);
    gulp.watch('scripts/*.js', ['jsscripts', 'jsscripts_admin']);
    gulp.watch('**/*.html').on('change', reload);
    gulp.watch('**/*.php').on('change', reload);
    gulp.watch('./asset_source.js', ['csslibs', 'jslibs']);
});

gulp.task('browser-sync', function () {
    var files = [
        './css/*.css',
        './js/*.js',
        './scripts/*.js',
        '**/*.php',
        '**/*.html',
        './images/**/*.{png,jpg,gif,svg,webp}',
        './asset_source.js'
    ];
    browserSync.init({
        proxy: {
            target: LOCAL_SITE
        },
        open: 'local',
        files: files,
        port: 4000,
        browser: ['google chrome'],
        notify: true,
        reloadOnRestart: true,
        ui: false
    });
});

// clean files for building
gulp.task('clean', function () {
    return gulp.src(inputs.clean, {
            read: false
        })
        .pipe(clean())
        .pipe(notify({
            message: 'Dev files cleaned !'
        }));
});
gulp.task('clean_libs', function () {
    return gulp.src(inputs.clean_libs, {
            read: false
        })
        .pipe(clean())
        .pipe(notify({
            message: 'Dev files cleaned !'
        }));
});
gulp.task('cleanall', function () {
    return gulp.src(inputs.cleanall, {
            read: false
        })
        .pipe(clean())
        .pipe(notify({
            message: 'Dev files cleaned !'
        }));
});
// for production
var fixlinks = [
    ['js/jtlbapp.js', 'js/jtlbapp.min.js'],
    ['js/admin.js', 'js/admin.min.js'],
    ['css/libs.css', 'css/libs.min.css'],
    ['css/app.css', 'css/app.min.css']
];
gulp.task('fixlinksIndex', function () {
    return gulp.src('jtlb-theater-listing.php') // change this according to need for build.alt: can be put in asset_source.js file as well
        .pipe(replaceBatch(fixlinks))
        .pipe(gulp.dest('./'));
});
gulp.task('fixlinks', ['fixlinksIndex'], function () {
    return gulp.src('inc/shortcode-fe.php') // change this according to need for build.alt: can be put in asset_source.js file as well
        .pipe(replaceBatch(fixlinks))
        .pipe(gulp.dest('inc'));
});

// for dev env
var devlinks = [
    ['js/jtlbapp.min.js', 'js/jtlbapp.js'],
    ['js/admin.min.js', 'js/admin.js'],
    ['css/libs.min.css', 'css/libs.css'],
    ['css/app.min.css', 'css/app.css']
];
gulp.task('devlinksIndex', function () {
    return gulp.src('jtlb-theater-listing.php') // change this according to need for build.alt: can be put in asset_source.js file as well
        .pipe(replaceBatch(devlinks))
        .pipe(gulp.dest('./'));
});
gulp.task('devlinks', ['devlinksIndex'], function () {
    return gulp.src('inc/shortcode-fe.php') // change this according to need for build.alt: can be put in asset_source.js file as well
        .pipe(replaceBatch(devlinks))
        .pipe(gulp.dest('inc'));
});


// first start in dev env
gulp.task('first-run', gulpSequence('cleanall', ['sass', 'csslibs', 'admin-sass'], ['jslibs', 'jsscripts', 'jsscripts_admin', 'devlinks']));

// default task
gulp.task('default', ['browser-sync', 'watch']);

// task to run after editing asset_soruce.js
gulp.task('edit-source', gulpSequence('clean_libs', ['csslibs', 'jslibs', 'jsscripts_admin', 'admin-sass', 'sass']));

// build
gulp.task('prep-dist', gulpSequence(['fixlinks', 'cleanall'], 'minify-sass', 'admin-sass', 'minify-csslibs', 'jslibs', 'minify-js', 'jsscripts_admin'));

gulp.task('build', ['prep-dist'], function () {
    return gulp.src([
            './**',
            '!./bower_components{,/**}',
            '!./node_modules{,/**}',
            '!./asset_source.js',
            '!./bower.json',
            '!./gulpfile.js',
            '!./package.json',
            '!./package-lock.json',
            '!./css/app.css',
            '!./css/libs.css',
            '!./js/jtlbapp.js',
            '!./js/admin.js',
            '!./scripts{,/**}',
            '!./scss{,/**}',
            '!./srcmaps{,/**}',
            '!./README.md'
        ], {
            base: '.'
        })
        .pipe(gulp.dest('../jtlb-theater-listing-dist/'));
});