/*
 * Custom scripts for the plugin
 * Author: ajasra das
 */

var $ = jQuery.noConflict();

// if (typeof jQuery != 'undefined') {
//     console.log(jQuery.fn.jquery);
// }

// const buttonData = (WPURLS.buttonData.length > 0) ? WPURLS.buttonData[0] : '';

// if (buttonData) {
//     let style = JSON.parse(buttonData.style_json);
//     let movie = JSON.parse(buttonData.movie_json);
// }


// // custom sorting function to sort stuff alphabetically
// function sorting(json_object, key_to_sort_by) {
//     function sortByKey(a, b) {
//         var x = a[key_to_sort_by];
//         var y = b[key_to_sort_by];
//         return ((x < y) ? -1 : ((x > y) ? 1 : 0));
//     }

//     json_object.sort(sortByKey);
// }


// console.log(WPURLS.cinemalist_obj);

let masterData = WPURLS.cinemalist_obj;

// create the region select
let regionSelect = $('#jtlb-region'),
    citySelect = $('#jtlb-city'),
    cinemaWrap = $('#jtlb-cinema');

// order the regions alphabetically
let orderedRegions = _.orderBy(masterData.regions, ['text'], ['asc']);

$(window).resize(() => {
    if ($(window).width() < 768) {
        regionSelect.select2({
            placeholder: 'Nome regione',
            data: orderedRegions,
            language: 'it',
            minimumResultsForSearch: -1
        });
    } else {
        regionSelect.select2({
            placeholder: 'Nome regione',
            data: orderedRegions,
            language: 'it'
        });
    }
}).resize();

regionSelect.on('change', function() {

    let selectedRegion = $(this).val();

    // if region is selected create city select
    if (selectedRegion) {
        let cities = _.filter(masterData.cities, {
            'region_id': selectedRegion,
        });

        let orderedCities = _.orderBy(cities, ['text'], ['asc']);
        
        // clear the select2
        citySelect.val(null).trigger('change');
        citySelect.html('<option></option>');

        if ($(window).width() < 768) {
            citySelect.select2({
                placeholder: 'Nome città',
                data: orderedCities,
                language: 'it',
                minimumResultsForSearch: -1
            });
        } else {
            citySelect.select2({
                placeholder: 'Nome città',
                data: orderedCities,
                language: 'it'
            });
        }

        citySelect.parents('.city-select').fadeIn();

        // create cinems on city select
        citySelect.on('change', function() {
            let selectedCity = $(this).val();
            cinemaWrap.find('ul.cinema').html('').parents('.cinema-select').hide();

            // trigger custom event
            cinemaWrap.trigger('cinemachange');

            setTimeout(() => {
                // if city is selected 
                if (selectedCity) {
                    cinemaWrap.parents('.cinema-select').fadeIn();

                    let cinemas = _.filter(masterData.cinemas, {
                        'city_id': selectedCity,
                    });

                    let orderedCinemas = _.orderBy(cinemas, ['name'], ['asc']);

                    let cinemaData = '';
                    orderedCinemas.forEach(elem => {
                        let link = (elem.url === '#') ? 'class="no-link"' : `href="${elem.url}"`;
                        cinemaData += `
                        <li><a ${link} target="_blank">${elem.name}</a></li>
                    `;
                    });

                    cinemaWrap.find('ul.cinema').html(cinemaData);

                    // console.log(selectedCity);

                    // trigger custom event
                    cinemaWrap.trigger('cinemaAdded');
                } else {
                    // clear cinema wrap
                    cinemaWrap.find('ul.cinema').html('').parents('.cinema-select').fadeOut();
                }
            }, 100);
        });

    } else {
        citySelect.parents('.city-select').fadeOut();
        cinemaWrap.parents('.cinema-select').fadeOut();
    }
});
