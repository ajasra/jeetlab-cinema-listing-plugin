/*
 * Custom scripts for the plugin
 * Author: ajasra das
 */

var $ = jQuery.noConflict();

if (typeof jQuery != 'undefined') {
    console.log(jQuery.fn.jquery);
}

// shortcodes
let shortcodes = {
    init: function() {
        this.dataTable();
    },
    dataTable: function() {
        let table = $('#shortcode-table').DataTable({
            // processing: true,
            ajax: {
                url: WPURLS.listShortCodes,
                type: 'POST',
                dataSrc: ''
            },
            columns: [
                {
                    title: '#',
                    data: 'id'
                },
                {
                    title: 'Name',
                    data: 'name'
                },
                {
                    title: 'Shortcode',
                    data: 'shortcode'
                },
                {
                    title: '',
                    data: 'actions'
                }
            ],
            columnDefs: [
                {
                    targets: [0],
                    visible: false
                },
                {
                    targets: [2],
                    width: '40%'
                },
                {
                    targets: [3],
                    width: '150px',
                    orderable: false,
                }
            ]
        });

        // remove the shortcode
        table.on('click', '.btn.remove', function (e) {
            e.preventDefault();

            let t = $(this),
                conf = confirm('Are you sure you want to remove the shortcode from the database? This process cannot be restored.');

            if (conf === true) {
                $.ajax({
                    url: WPURLS.ajaxurl,
                    data: {
                        'action': 'jtlb_remove_cinema_shortcode',
                        'entry_id': t.data('id')
                    },
                    type: 'POST',
                    beforeSend: function (xhr) {
                        t.addClass('is-loading');
                    },
                    success: function (data) {
                        if (data) {
                            t.removeClass('is-loading');

                            table.ajax.reload();
                        }
                    },
                    error: function (data) {
                        table.ajax.reload();
                    }
                });
            }
        });
    },
    viewInit: function() {
        this.cloneCinemas();
        $(window).one('load', function() {
            shortcodes.select();
        });
    },
    cloneCinemas: function() {
        let form = $('#shortcode-form');

        let i = $('.cinemas').find('.cinema-clone').size() + 1;

        // clone the selection blocks
        $('.add-cinema').on('click', function(e) {
            e.preventDefault();

            let t = $(this);

            let clone = `
            <div class="col-6 cinema-clone new">
                <div class="row align-items-center form-group">
                    <div class="col">
                        <div class="">
                            <select class="form-control cinema" name="cinemas[]">
                                <option></option>
                            </select>
                            
                            <input type="text" class="form-control url" name="url[]" placeholder="URL of cinema"> 
                        </div>    
                    </div>
                    <div class="col-auto">
                        <button class="btn btn-danger jt-remove">
                            <i class="far fa-times-circle"></i>
                        </button>
                    </div>
                </div>
            </div>
            `;     

            // get last child select value 
            let lastClone = $('.cinemas').find('.cinema-clone:last-child'),
                selectVal = lastClone.find('.form-control.cinema').val();

            if (selectVal) {
                $('.cinemas').append(clone);

                i++;

                shortcodes.selectNew();
            } else {
                let msg = '<span class="msg text-danger">Cannot add new Cinema with a blank previous.</span>';

                $(msg).insertAfter(t);
                
                setTimeout(() => {
                    t.siblings('.msg').fadeOut().delay(500).remove();
                }, 2500);
            }
        });

        // remove clones
        $('.cinemas').on('click', '.jt-remove', function (e) {
            e.preventDefault();

            if (i > 2) {
                $(this).parents('.cinema-clone').remove();
                i--;
            }
            return false;
        });

        $('#save').click(function(e) {
            e.preventDefault();

            let loading = form.find('.jtlb-loading'),
                t = $(this);

            $.ajax({
                url: WPURLS.ajaxurl,
                data: {
                    'action': 'jtlb_edit_cinema_shortcode',
                    'formData': form.serialize()
                },
                type: 'POST',
                beforeSend: function() {
                    t.addClass('disabled');
                    loading.fadeIn();
                    // console.log(form.serializeArray());
                },
                success: function (response) {
                    t.removeClass('disabled');
                    loading.fadeOut();
                    
                    // check if new post or existing post
                    if (response.data[0].id) {
                        let msg = 'Shortcode saved successfully. This page will refresh in 4 seconds';
                        $('.form-area > .response .updated').html(msg).fadeIn();

                        setTimeout(() => {
                            location.href = response.data[0].url;
                        }, 4000);
                    } else {
                        let msg = '';
                        response.data.map((val) => {
                            msg += val + '<br>';
                        });

                        if (response.success === true) {
                            $('.form-area > .response .updated').html(msg).fadeIn();
                        } else {
                            $('.form-area > .response .error').html(msg).fadeIn();
                        }
                    }
                }
            });

            return false;
        });

    },
    select: function() {
        let form = $('#shortcode-form');
        let values = form.find('.cinema[name="cinemas[]"]').map((i, elem) => {
            return $(elem).val();
        }).get();
        
        form.find('.cinema').each(function () {
        
            let t = $(this);

            $.ajax({
                url: WPURLS.ajaxurl,
                data: {
                    'action': 'jtlb_get_cinemas_for_shortcodes',
                    'ids': values.toString()
                },
                type: 'POST',
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    t.closest('.input-group').addClass('loading');
                },
                success: function (response) {
                    if (response) {

                        t.select2({
                            placeholder: 'Select cinema',
                            data: response
                        });

                        // get the cinema url from db
                        t.on('change', function() {
                            let selectedId = $(this).val(),
                                selectedObj = _.filter(response, { 'id': selectedId });

                            $(this).siblings('.form-control.url').val(selectedObj[0].url);
                        });

                        t.closest('.input-group').removeClass('loading');
                    }
                }
            });
        });
    },
    selectNew: function() {
        let form = $('#shortcode-form');
        let values = form.find('.cinema[name="cinemas[]"]').map((i, elem) => {
            return $(elem).val();
        }).get();
        
        let newSelect = form.find('.cinema-clone.new'),
            t = newSelect.find('select.cinema');

        $.ajax({
            url: WPURLS.ajaxurl,
            data: {
                'action': 'jtlb_get_cinemas_for_shortcodes',
                'ids': values.toString()
            },
            type: 'POST',
            cache: false,
            dataType: 'json',
            beforeSend: function () {
                t.closest('.input-group').addClass('loading');
            },
            success: function (response) {
                if (response) {

                    t.select2({
                        placeholder: 'Select cinema',
                        data: response
                    });

                    // get the cinema url from db
                    t.on('change', function() {
                        let selectedId = $(this).val(),
                            selectedObj = _.filter(response, { 'id': selectedId });

                        $(this).siblings('.form-control.url').val(selectedObj[0].url);
                    });

                    t.closest('.input-group').removeClass('loading');

                    newSelect.removeClass('new');
                }
            }
        });
    },
};



let regions = {
    init: function() {
        this.dataTable();
    },
    dataTable: function() {
        let table = $('#regions-table').DataTable({
            processing: true,
            ajax: {
                url: WPURLS.listRegions,
                dataSrc: ''
            },
            columns: [
                {title: '', data: 'id'},
                {title: 'Region', data: 'region'},
                {title: '', data: 'active'},
                {title: '', data: 'edit'}
            ],
            order: [1, 'asc'],
            columnDefs: [
                {
                    targets: [0],
                    visible: false
                },
                {
                    targets: [2, 3],
                    orderable: false,
                    className: 'dt-center'
                }
            ]
        });

        // add regions
        regions.add(table);

        // edit regions
        regions.edit(table);

        // remove regions
        regions.remove(table);
    },
    add: function(table) {
        // add region form
        let addRegionForm = $('.add-region-form > form'),
            btn = addRegionForm.find('button[type="submit"]');

        addRegionForm.submit(function () {
            $('.add-region-form').find('.alert').remove();

            $.ajax({
                type: 'POST',
                url: WPURLS.ajaxurl,
                data: $(this).serializeArray(),
                beforeSend: function (xhr) {
                    btn.find('i.fas').attr({
                        'class': 'fas fa-circle-notch fa-spin'
                    });
                    btn.addClass('loading');
                },
                success: function (response) {
                    btn.find('i.fas').attr({
                        'class': 'fas fa-plus-circle'
                    });
                    btn.removeClass('loading');

                    let alert;
                    if (response.success) {
                        alert = `
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Well done!</strong> ${response.data}
                        </div>`;
                        addRegionForm[0].reset();
                    } else {
                        alert = `
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Oops!</strong> ${response.data}
                        </div>`;
                    }

                    $('.add-region-form').prepend(alert);

                    table.ajax.reload();
                }
            });
            return false;
        });
    },
    edit: function(table) {
        // edit region from table
        table.on('click', 'button.edit', function () {

            $('#regions-table').find('button.edit').addClass('disabled').css('pointer-events', 'none');

            $('#region-edit').fadeIn()
                .find('input[name="region"]').val($(this).data('region'))
                .parents('form')
                .find('input[name="region_id"]').val($(this).data('id'))
                .parents('form')
                .find('input[name="old_region"]').val($(this).data('region'));
        });

        // edit form
        let form = $('#region-edit form');
        form.submit(function () {
            $('#region-edit').find('.alert').remove();
            $.ajax({
                type: 'POST',
                url: WPURLS.ajaxurl,
                data: $(this).serializeArray(),
                success: function (response) {
                    if (response.success) {
                        $('#region-edit .panel-body').prepend(`
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>Well done!</strong> ${response.data}
                            </div>
                        `);
                        setTimeout(() => {
                            form[0].reset();
                            $('#region-edit').fadeOut();
                            table.ajax.reload();
                        }, 1000);
                    } else {
                        $('#region-edit .panel-body').prepend(`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Oops!</strong> ${response.data}
                        </div>`);
                    }
                }
            });
            return false;
        });

        // close the edit form
        $('#region-edit .close').click(function (e) {
            e.preventDefault();
            $('#region-edit').fadeOut().find('.alert').remove();
            form[0].reset();
            table.ajax.reload();
        });
    },
    remove: function(table) {
        // remove region from table
        table.on('click', 'button.show-hide', function () {
            let btn = $(this).button('loading');
            $.ajax({
                type: 'POST',
                url: WPURLS.ajaxurl,
                data: {
                    action: 'jtlb_hide_region',
                    id: $(this).data('id'),
                    active_flag: $(this).data('active')
                },
                success: function (response) {
                    table.ajax.reload();
                    setTimeout(function () {
                        btn.button('reset');
                    }, 1000);
                }
            });
        });
    }
};


let cities = {
    init: function() {
        this.regionSelect();
        this.dataTable();
    },
    regionSelect: function() {
        let data = {
            'active': true
        };
        $.getJSON(WPURLS.listRegions,
            data,
            function (item) {
                $.each(item, function (k, val) {
                    $('.ajax-region-select').append(`<option value="${val.id}">${val.region}</option>`);
                });

                $('.ajax-region-select').select2({
                    placeholder: 'Select a Region'
                });
            });
    },
    dataTable: function() {
        let table = $('#cities-table').DataTable({
            processing: true,
            ajax: {
                url: WPURLS.listCities,
                dataSrc: ''
            },
            columns: [{
                    title: '',
                    data: 'id'
                },
                {
                    title: 'City',
                    data: 'city'
                },
                {
                    title: 'Region',
                    data: 'region'
                },
                {
                    title: '',
                    data: 'active'
                },
                {
                    title: '',
                    data: 'edit'
                }
            ],
            order: [1, 'asc'],
            columnDefs: [{
                targets: [0],
                visible: false
            }]
        });

        // add cities
        cities.add(table);

        // edit cities
        cities.edit(table);

        // remove cities
        cities.remove(table);
    },
    add: function(table) {
        // add city form
        let addCityForm = $('.add-city-form > form'),
            btn = addCityForm.find('button[type="submit"]');

        addCityForm.submit(function () {
            $('.add-city-form').find('.alert').remove();
            $.ajax({
                type: 'POST',
                url: WPURLS.ajaxurl,
                data: $(this).serializeArray(),
                beforeSend: function (xhr) {
                    btn.find('i.fas').attr({
                        'class': 'fas fa-circle-notch fa-spin'
                    });
                    btn.addClass('loading');
                },
                success: function (response) {
                    btn.find('i.fas').attr({
                        'class': 'fas fa-plus-circle'
                    });
                    btn.removeClass('loading');

                    let alert;
                    if (response.success) {
                        alert = `
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Well done!</strong> ${response.data}
                        </div>`;
                        
                        addCityForm[0].reset();
                        $('.ajax-region-select').val('').trigger('change');

                    } else {
                        alert = `
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Oops!</strong> ${response.data}
                        </div>`;
                    }

                    $('.add-city-form').prepend(alert);

                    table.ajax.reload();
                }
            });

            return false;
        });
    },
    edit: function(table) {
        // edit City
        table.on('click', 'button.edit', function () {
            $('#cities-table').find('button.edit').addClass('disabled').css('pointer-events', 'none');
            $('#cities-edit').fadeIn()
                .find('input[name="city"], input[name="old_city"]').val($(this).data('city'))
                .parents('form')
                .find('input[name="city_id"]').val($(this).data('id'))
                .parents('form')
                .find('input[name="region_id"]').val($(this).data('region_id'))
                .parents('form')
                .find('input[name="old_region"]').val($(this).data('region'))
                .parents('form')
                .find('.ajax-region-select').select2().val($(this).data('region_id')).trigger('change');
        });

        // edit form
        let form = $('#cities-edit form');
        form.submit(function () {
            $('#cities-edit').find('.alert').remove();
            $.ajax({
                type: 'POST',
                url: WPURLS.ajaxurl,
                data: $(this).serializeArray(),
                success: function (response) {
                    if (response.success) {
                        $('#cities-edit .panel-body').prepend(`
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>Well done!</strong> ${response.data}
                            </div>`);

                        setTimeout(() => {
                            form[0].reset();
                            $('#cities-edit').fadeOut();
                            table.ajax.reload();
                        }, 1000);
                    } else {
                        $('#cities-edit .panel-body').prepend(`
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>Oops!</strong> ${response.data}
                            </div>`);
                    }
                }
            });
            return false;
        });

        $('#cities-edit .close').click(function (e) {
            e.preventDefault();
            $('#cities-edit').fadeOut().find('.alert').remove();
            form[0].reset();
            table.ajax.reload();
        });
    },
    remove: function(table) {
        // remove city from table
        table.on('click', 'button.show-hide', function () {
            let btn = $(this).button('loading');
            $.ajax({
                type: 'POST',
                url: WPURLS.ajaxurl,
                data: {
                    action: 'jtlb_hide_city',
                    id: $(this).data('id'),
                    active_flag: $(this).data('active')
                },
                success: function (response) {
                    table.ajax.reload();
                    setTimeout(function () {
                        btn.button('reset');
                    }, 1000);
                }
            });
        });
    }
};


let cinemas = {
    init: function() {
        this.citySelect();
        this.dataTable();
    },
    citySelect: function() {
        // populating the city list
        $.getJSON(WPURLS.listCities, {
                    'active': true
                },
                function (item) {
                    $.each(item, function (k, val) {
                        $('.ajax-city-select').append(`<option value="${val.id}">${val.city}</option>`);
                    });

                    $('.ajax-city-select').select2({
                        placeholder: 'Select a City'
                    });
                });
    },
    dataTable: function() {
        // render datatables
        let table = $('#cinemas-table').DataTable({
            ajax: {
                url: WPURLS.listCinemas,
                dataSrc: ''
            },
            columns: [{
                    title: 'ID',
                    data: 'id'
                },
                {
                    title: 'Cinema',
                    data: 'cinema'
                },
                {
                    title: 'City',
                    data: 'city'
                },
                {
                    title: 'Region',
                    data: 'region'
                },
                {
                    title: 'URL',
                    data: 'url'
                },
                {
                    title: '',
                    data: 'active'
                },
                {
                    title: '',
                    data: 'edit'
                },
                {
                    title: '',
                    data: 'del'
                },
            ],
            order: [1, 'asc'],
            columnDefs: [
                {
                    targets: [0],
                    visible: false
                }, {
                    targets: [6,7],
                    class: 'dt-center',
                    orderable: false,
                }, {
                    targets: [5],
                    class: 'dt-center',
                },
            ]
        });

        // add cinema
        cinemas.add(table);

        // edit cinema
        cinemas.edit(table);

        // delete cinema
        cinemas.delete(table);

        // hide cinema
        cinemas.hide(table);
    },
    add: function(table) {
        // add cinemas
        let addCinemaForm = $('.add-cinema-form > form'),
            btn = addCinemaForm.find('button[type="submit"]');

        addCinemaForm.submit(function () {
            $('.add-cinema-form').find('.alert').remove();
            $.ajax({
                type: 'POST',
                url: WPURLS.ajaxurl,
                data: $(this).serializeArray(),
                beforeSend: function (xhr) {
                    btn.find('i.fas').attr({
                        'class': 'fas fa-circle-notch fa-spin'
                    });
                    btn.addClass('loading');
                },
                success: function (response) {
                    btn.find('i.fas').attr({
                        'class': 'fas fa-plus-circle'
                    });
                    btn.removeClass('loading');

                    let alert;
                    if (response.success) {
                        alert = `
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Well done!</strong> ${response.data}
                        </div>`;

                        addCinemaForm[0].reset();
                        $('.ajax-city-select').val('').trigger('change');

                    } else {
                        alert = `
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Oops!</strong> ${response.data}
                        </div>`;
                    }
                    
                    $('.add-cinema-form').prepend(alert);

                    table.ajax.reload();
                }
            });

            return false;
        });
    },
    edit: function(table) {
        // edit cinema
        table.on('click', 'button.edit', function () {
            $('#cinemas-table tbody').find('button.edit').addClass('disabled').css('pointer-events', 'none');
            $('#cinemas-edit').find('.alert').remove();

            $('#cinemas-edit').fadeIn()
                .find('input[name="cinema"], input[name="old_cinema"]').val($(this).data('cinema'))
                .parents('form')
                .find('input[name="cinema_id"]').val($(this).data('id'))
                .parents('form')
                .find('input[name="old_city"]').val($(this).data('city_id'))
                .parents('form')
                .find('input[name="ext_url"]').val($(this).data('ext_url'))
                .parents('form')
                .find('.ajax-city-select').select2().val($(this).data('city_id')).trigger('change');
        });

        // edit form
        let form = $('#cinemas-edit form');
        form.submit(function () {
            $('#cinemas-edit').find('.alert').remove();
            
            $.ajax({
                type: 'POST',
                url: WPURLS.ajaxurl,
                data: $(this).serializeArray(),
                success: function (response) {
                    if (response.success) {
                        $('#cinemas-edit .panel-body').prepend(`
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>Well done!</strong> ${response.data}
                            </div>`);

                        setTimeout(() => {
                            form[0].reset();
                            $('#cinemas-edit').fadeOut();
                            table.ajax.reload();
                        }, 2000);
                    } else {
                        $('#cinemas-edit .panel-body').prepend(`
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>Oops!</strong> ${response.data}
                            </div>`);
                    }
                }
            });
            return false;
        });

        $('#cinemas-edit .close').click(function (e) {
            e.preventDefault();
            $('#cinemas-edit').fadeOut().find('.alert').remove();
            form[0].reset();
            table.ajax.reload();
        });
    },
    delete: function(table) {
        // remove cinema
        table.on('click', 'button.delete', function () {
            let id = $(this).data('id');
            let cinema = $(this).data('cinema');

            let dialog = `Are you sure you want to remove the Cinema - ${cinema} ?`;

            if (confirm(dialog) == true) {
                $.ajax({
                    type: 'POST',
                    url: WPURLS.ajaxurl,
                    data: {
                        action: 'jtlb_delete_cinema',
                        id: id,
                    },
                    success: function (response) {
                        table.ajax.reload();
                    }
                });
            }
        });
    },
    hide: function(table) {
        // remove cienma from table
        table.on('click', 'button.show-hide', function () {
            let btn = $(this).button('loading');

            $.ajax({
                type: 'POST',
                url: WPURLS.ajaxurl,
                data: {
                    action: 'jtlb_hide_cinema',
                    id: $(this).data('id'),
                    active_flag: $(this).data('active')
                },
                success: function (response) {
                    table.ajax.reload();
                }
            });
        });
    }
};