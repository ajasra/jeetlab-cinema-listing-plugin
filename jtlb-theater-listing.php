<?php
/*
    Plugin Name: Jeetlab Cinema Listing
    Plugin URI: jeetlab.com
    Description: Custom plugin to list cinema theaters for the movie by Jeetlab
    Author: Ajasra Das
    Author URI: jeetlab.com
    Version: 3.0.0
    Text Domain: jtlb
*/
/* 
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

    Copright 2019 Ajasra Das(das.ajasra@gmail.com) and Jeetlab.com
*/

// Prevent loading this file directly
defined('ABSPATH') or die('Don\'t even think of doing this :-)');

if (!class_exists('JtlbCinemaListing')) {

    class JtlbCinemaListing
    {
        function __construct() {
            add_action( 'init', array( 'JtlbCinemaListing', 'custom_post_type' ) );
        }

        // method to register the plugin
        public function register_plugin() {
            // add the scripts and styles
            add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts_styles' ) );

            // add the admin pages
            add_action( 'admin_menu', array( $this, 'add_admin_pages') );

            // ajax hooks
            require_once plugin_dir_path( __FILE__ ) . 'inc/ajax-list.php';
            JtlbAjaxListing::resgister_the_hooks();

            require_once plugin_dir_path( __FILE__ ) . 'inc/ajax-shorcode-crud.php';
            JtlbAjaxShortcodeCrud::resgister_the_hooks();
            
            require_once plugin_dir_path( __FILE__ ) . 'inc/ajax-data-crud.php';
            JtlbSeedDataCrud::resgister_the_hooks();

            // call the shortcode action
            require_once plugin_dir_path( __FILE__ ) . 'inc/shortcode-fe.php';
            JtlbShortcodeRender::shortcode_action();
        }

        // add the admin pages
        public function add_admin_pages() {
            $menuPage = add_menu_page('Cinema Listing', 'Cinema Listing', 'edit_pages', 'cinema_listing', array( $this, 'cinema_dashboard'), 'dashicons-video-alt', 35);
            $regionPage = add_submenu_page('cinema_listing', 'Manage Regions', 'Manage Regions', 'edit_pages', 'cinema_listing_region', array( $this, 'cinema_listing_region'));
            $cityPage = add_submenu_page('cinema_listing', 'Manage Cities', 'Manage Cities', 'edit_pages', 'cinema_listing_city', array( $this, 'cinema_listing_city' ));
            $cinemaPage = add_submenu_page('cinema_listing', 'Manage Cinemas', 'Manage Cinemas', 'edit_pages', 'cinema_listing_cinema', array( $this, 'cinema_listing_cinema' ));
            $viewShortcode = add_submenu_page('', 'View Shortcode', 'View Shortcode', 'edit_pages', 'cinema_shortcode_view', array( $this, 'cinema_view_shortcode' ));
            $addShortcode = add_submenu_page('', 'Add Shortcode', 'Add Shortcode', 'edit_pages', 'cinema_shortcode_new', array( $this, 'cinema_new_shortcode' ));
        }

        public function cinema_dashboard() {
            require_once dirname(__FILE__) . '/template/admin/dashboard.php';
        }
        // region page
        function cinema_listing_region() {
            require_once dirname(__FILE__) . '/template/admin/region.php';
        }
        // city page
        function cinema_listing_city() {
            require_once dirname(__FILE__) . '/template/admin/city.php';
        }
        // cinem page
        function cinema_listing_cinema() {
            require_once dirname(__FILE__) . '/template/admin/cinema.php';
        }
        // shortcode view
        function cinema_view_shortcode() {
            require_once dirname(__FILE__) . '/template/admin/view_shortcode.php';
        }
        // shortcode add
        function cinema_new_shortcode() {
            require_once dirname(__FILE__) . '/template/admin/add_shortcode.php';
        }

        // create custom post type for the theater combinations
        static function custom_post_type() {
            $labels = array(
                'name' => _x('Cinema Listing', 'post type general name', 'jeetlab-framework'),
                'singular_name' => _x('Cinema Listing', 'post type singular name', 'jeetlab-framework'),
                'add_new' => _x('Add new Cinema Listing', 'cinema_listing', 'jeetlab-framework'),
                'add_new_item' => __('Add new Cinema Listing', 'jeetlab-framework'),
                'edit_item' => __('Modify Cinema Listing', 'jeetlab-framework'),
                'new_item' => __('New Cinema Listing', 'jeetlab-framework'),
                'view_item' => __('View Cinema Listing', 'jeetlab-framework'),
                'search_items' => __('Search Cinema Listings', 'jeetlab-framework'),
                'not_found' => __('No Cinema Listings found', 'jeetlab-framework'),
                'not_found_in_trash' => __('No Cinema Listings found in trash', 'jeetlab-framework'),
                'parent_item_colon' => '',
                'menu_name' => 'Cinemas'
            );
            $args = array(
                'labels' => $labels,
                'public' => true,
                'publicly_queryable' => false,
                'exclude_from_search' => true,
                'show_ui' => true,
                'show_in_menu' => false,
                'query_var' => false,
                'rewrite' => false,
                'capability_type' => 'post',
                'has_archive' => true,
                'menu_icon' => 'dashicons-video-alt',
                'hierarchical' => false,
                'menu_position' => 30,
                'supports' => array('title')
            );
            register_post_type('jtlb-cinema', $args);
        }

        // remove misc publishing actions from the new post type
        function modify_misc_publish_actions() {
            $jeet_post_type = 'jtlb-cinema';
            global $post;
            if($post->post_type == $jeet_post_type){
            echo '<style type="text/css">
                    #misc-publishing-actions,
                    #minor-publishing-actions{
                    display:none;
                }
                </style>';
            }
        }

        function jeet_remove_misc_publish_actions() {
            add_action('admin_head-post.php', array( $this, 'modify_misc_publish_actions' ));
            add_action('admin_head-post-new.php', array( $this, 'modify_misc_publish_actions' ));
        }

        // enqueue styles and scritps
        function enqueue_scripts_styles($hook) {

            // echo '<pre style="margin-left: 200px;">';
            // var_dump($hook);
            // echo '</pre>';

            $hook_array = array(
                'toplevel_page_cinema_listing',
                'cinema-listing_page_cinema_listing_region',
                'cinema-listing_page_cinema_listing_city',
                'cinema-listing_page_cinema_listing_cinema',
                'admin_page_cinema_shortcode_view',
                'admin_page_cinema_shortcode_new'
            );

            if (!in_array($hook, $hook_array)) {
                return;
            }

            wp_enqueue_style('plugin-libs', plugins_url('css/libs.css', __FILE__), '');
            wp_enqueue_style('datatables-css', '//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css', array(), '', 'all');
            wp_enqueue_style('plugin-select2', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', '4.0.6');
            wp_enqueue_style('plugin-select2_bs', '//cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css', '0.1.0');
            wp_enqueue_style('admin-fontawesome', '//use.fontawesome.com/releases/v5.5.0/css/all.css', array(), '', 'all');
            wp_enqueue_style('cinema-css', plugins_url('css/app.css', __FILE__), '1.0.0');


            
            wp_enqueue_script('plugin-bs', plugins_url('js/vendor/bootstrap.min.js', __FILE__), '', '', false);
            wp_enqueue_script('plugin-lodash', plugins_url('js/vendor/lodash.min.js', __FILE__), '', '', false);
            wp_enqueue_script('datatables', '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js', '', '', false);
            wp_enqueue_script('datatables-buttons', '//cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js', '', '', false);
            wp_enqueue_script('datatables-jszip', '//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js', '', '', false);
            wp_enqueue_script('datatables-pdfmake', '//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js', '', '', false);
            wp_enqueue_script('datatables-vfs_fonts', '//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js', '', '', false);
            wp_enqueue_script('datatables-btns-html5', '//cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js', '', '', false);
            wp_enqueue_script('datatables-btns-print', '//cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js', '', '', false);
            // wp_enqueue_script('cinema-datatables_bs', '//cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap.min.js', '', '1.10.19', false);
            wp_enqueue_script('cinema-select2', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', '', '4.0.6', false);
            wp_enqueue_script('cinema-admin-js', plugins_url('js/admin.js', __FILE__), '', '1.0.0', false);

            wp_localize_script('cinema-admin-js', 'WPURLS', array(
                'ajaxurl'       => admin_url('admin-ajax.php'),
                'listRegions'   => admin_url('admin-ajax.php?action=jtlb_list_regions'),
                'listCities'    => admin_url('admin-ajax.php?action=jtlb_list_cities'),
                'listCinemas'   => admin_url('admin-ajax.php?action=jtlb_list_cinemas'),
                'listShortCodes' => admin_url('admin-ajax.php?action=jtlb_list_shortcodes'),
            ));
        }
    }



    $jtlbCinemaListing = new JtlbCinemaListing();

    // call the enqueue script when plguin is active
    $jtlbCinemaListing->register_plugin();

    $jtlbCinemaListing->jeet_remove_misc_publish_actions();


    // activation of plugin
    require_once plugin_dir_path( __FILE__ ) . 'inc/class.activate.php';
    register_activation_hook( __FILE__ , array( 'JtlbCinemaPluginActivate', 'activate' ) );

    // deactivation of plugin
    require_once plugin_dir_path( __FILE__ ) . 'inc/class.deactivate.php';
    register_deactivation_hook( __FILE__ , array( 'JtlbCinemaPluginDectivate', 'deactivate' ) );
}


