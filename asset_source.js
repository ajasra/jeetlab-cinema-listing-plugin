module.exports = {
    libs: [
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/popper.js/dist/popper.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/lodash/dist/lodash.min.js',
        'bower_components/jquery-validation/dist/jquery.validate.min.js',
        'bower_components/select2/dist/js/select2.full.min.js'
    ],
    scripts: [
        'scripts/jtlbapp.js',
        'scripts/admin.js'
    ],
    libCss: [
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'bower_components/select2/dist/css/select2.css',
    ],
    scss: [
        'scss/app.scss'
    ],
    admin_scss: [
        'scss/admin.scss'
    ],
    cleanall: [
        'css/*',
        'js/*',
        'srcmaps'
    ],
    clean: [
        'css/app.css',
        'css/libs.css',
        'js/vendor/*',
        'js/jtlbapp.js',
        'js/admin.js',
        'srcmaps'
    ],
    clean_libs: [
        'css/libs.css',
        'js/vendor/*',
        'srcmaps'
    ]
};
